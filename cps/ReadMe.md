# Notes on SCOTS and the scs-files

The controller files in this folder were created using SCOTSv0.2, which is no longer maintained. A copy can be found here [https://github.com/mahendrasinghtomar/SCOTSv0.2_Copy](https://github.com/mahendrasinghtomar/SCOTSv0.2_Copy). [Mahmoud Khaled](https://www.hyconsys.com/members/mkhaled/) can still be contacted with questions regarding SCOTSv0.2. Note that the files cannot be created with the original [SCOTS](https://www.hyconsys.com/software/scots/).

An example on how to use SCOTSv0.2 to reproduce the 10rooms example can be found in the [respective folder](https://gitlab.lrz.de/i7/dtcontrol-examples/-/blob/master/cps/10rooms_additionalMaterial.zip).

